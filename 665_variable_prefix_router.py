import uvicorn
from fastapi import FastAPI, APIRouter

app = FastAPI()

device_router = APIRouter()

@device_router.get("/devices")
async def devices(deviceId: str):
    pass

account1 = "1"
account2 = "2"

for account in [account1, account2]:
    app.include_router(
        device_router,
        prefix=f"/{account}/devices"
    )


if __name__ == '__main__':
    uvicorn.run(app)