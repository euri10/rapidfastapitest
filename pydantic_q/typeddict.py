from pydantic import BaseModel
from typing_extensions import TypedDict

Point2D = TypedDict('Point2D', {'x': int, 'y': int, 'label': str})
a: Point2D = {'x': 1, 'y': 2, 'label': 'good'}  # OK
b: Point2D = {'z': 3, 'label': 'bad'}           # Fails type check


class Model(BaseModel):
    point2d: Point2D

m_a=Model(point2d=a)
m_b=Model(point2d=b)

print(m_a.dict())
print(m_b.dict())