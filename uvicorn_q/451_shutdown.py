import asyncio
from datetime import datetime

import uvicorn


class App:

    def __call__(self, scope):
        if scope['type'] == 'lifespan':
            return self.lifespan
        elif scope['type'] == 'http':
            return self.http

    async def lifespan(self, receive, send):
        """Handle lifespan messages"""

        message = await receive()
        assert message["type"] == "lifespan.startup"
        print('startup')
        await send({"type": "lifespan.startup.complete"})

        message = await receive()
        assert message["type"] == "lifespan.shutdown"
        print('shutdown')
        await send({"type": "lifespan.shutdown.complete"})

    async def http(self, receive, send):
        """Handle http messages"""
        message = await receive()
        assert message['type'] == 'http.request'

        while message['more_body']:
            message = await receive()

        await send({
            'type': 'http.response.start',
            'status': 200,
            'headers': [
                (b'cache-control', b'no-cache'),
                (b'content-type', b'text/event-stream'),
                (b'connection', b'keep-alive')
            ]
        })

        while True:
            print('here')
            await send({
                'type': 'http.response.body',
                'body': f'data: {datetime.now().isoformat()}\n\n'.encode('utf-8'),
                'more_body': True
            })

            try:
                # Check the receive, timing out after a second to send more data
                receive_task = asyncio.create_task(receive())
                await asyncio.wait_for(receive_task, 1)
                message = receive_task.result()
                if message['type'] == 'http.disconnect':
                    print('disconnect')
                    break
            except asyncio.TimeoutError:
                print('timeout')
                receive_task.cancel()
                await asyncio.sleep(0)


if __name__ == '__main__':
    uvicorn.run(App(), port=9009)