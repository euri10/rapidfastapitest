from os import PathLike
from typing import Union

from pydantic import BaseSettings
from functools import lru_cache
from dotenv import load_dotenv


class DBSettings(BaseSettings):
    host: str = "localhost"
    host_docker: str = "db"
    port: int = 5432
    user: str
    password: str
    db: str
    min_size: int = 10
    max_size: int = 10
    max_queries: int = 50000
    max_inactive_connection_lifetime: float = 300.0

    class Config:
        env_prefix = "POSTGRES_"


@lru_cache()
def get_db_settings(dotenv_path: Union[str, PathLike] = ".env") -> DBSettings:
    loaded = load_dotenv(dotenv_path=dotenv_path)
    return DBSettings()
