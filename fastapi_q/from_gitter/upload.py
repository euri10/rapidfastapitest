from typing import List

import uvicorn
from fastapi import FastAPI, File, UploadFile
from starlette.responses import HTMLResponse
from starlette.testclient import TestClient

app = FastAPI()


@app.post("/files/")
async def create_files(files: List[bytes] = File(...)):
    return {"file_sizes": [len(file) for file in files]}


@app.post("/uploadfiles/")
async def create_upload_files(files: List[UploadFile] = File(...)):
    return {"filenames": [file.filename for file in files]}


@app.get("/")
async def main():
    content = """
<body>
<form action="/files/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
<form action="/uploadfiles/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
</body>
    """
    return HTMLResponse(content=content)


def test_upload(tmp_path):
    url = app.url_path_for("create_upload_files")
    with TestClient(app) as client:
        f = tmp_path / 'fileupload'
        with open(f, 'wb') as tmp:
            tmp.write(b'upload this')
        with open(f, 'rb') as tmp:
            files = {'files': tmp}
            response = client.post(url, files=files)
            assert response.status_code == 200

if __name__ == '__main__':
    uvicorn.run("upload:app", reload=True)