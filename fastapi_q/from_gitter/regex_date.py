from datetime import datetime, date
from typing import List

import uvicorn
from fastapi import FastAPI, Query
from pydantic import BaseModel, validator
from starlette.testclient import TestClient

app = FastAPI()


class OrderFilter(BaseModel):
  from_date: datetime
  @validator('from_date', pre=True)
  def parse_from_date(cls, v):
      if isinstance(v, str):
          return datetime.strptime(v, "%Y-%M-%d")
      return v


@app.post("/datettt")
async def datettt_list(fd: OrderFilter):
    return fd


@app.get("/datefrom")
async def datefrom_list(from_date: date = Query(...)):
    return from_date


class Item(BaseModel):
    title: str


@app.get("/items/")
def read_items(q: List[Item] = Query(None)):
    pass  # pragma: no cover

def test_d():
    with TestClient(app) as client:
        response = client.get("/datefrom?from_date=77")
        assert response.status_code == 422
