# https://gitter.im/tiangolo/fastapi?at=5d83e422ab4244767bcca6eb

import asyncio
import logging
from functools import partial
from typing import Dict

import httpx
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.background import BackgroundTasks

logger: logging.Logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

app = FastAPI()


async def back_coro(url):
    async with httpx.AsyncClient(app=app, base_url="http://domain.tld") as client:
        response = await client.post(url, json={"amount": 5})
        assert response.status_code == 200
        logger.debug(f"response in startup from bckground {response.json()}")


@app.on_event("startup")
async def on_startup() -> None:
    logger.debug(f"on_event startup")
    url = app.url_path_for("sleepingtheback")
    item = Item(amount=5)
    # ian_task = asyncio.create_task(back_coro(url))
    ian_task = asyncio.create_task(
        sleepingtheback(
            item=item,
            background_tasks=BackgroundTasks(
                [partial(background_async, amount=item.amount)]
            ),
        )
    )
    await ian_task
    logger.debug(f"on_event startup END")


class Item(BaseModel):
    amount: int


async def background_async(amount: int) -> None:
    logger.debug("sleeping")
    await asyncio.sleep(amount)
    logger.debug("slept")


@app.post("/backgroundasync")
async def sleepingtheback(
    item: Item, background_tasks: BackgroundTasks
) -> Dict[str, str]:
    if not len(background_tasks.tasks):
        background_tasks.add_task(background_async, item.amount)
    else:
        await background_tasks()
    return {"message": f"sleeping {item.amount} in the back"}


if __name__ == "__main__":
    uvicorn.run(app=app)
