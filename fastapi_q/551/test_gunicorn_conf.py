import pytest
from gunicorn.app.base import BaseApplication
from main import app
from gunicorn_conf import GunicornSettings, WorkerEnum

conf_data = [
    {
        "GUNICORN_WORKER_CLASS": WorkerEnum.UvicornWorker,
        "GUNICORN_WORKERS": 2,
        "GUNICORN_THREADS": 1,
        "GUNICORN_BACKLOG": 2048,
    },
    {
        "GUNICORN_WORKER_CLASS": WorkerEnum.UvicornWorker,
        "GUNICORN_WORKERS": 3,
        "GUNICORN_THREADS": 1,
        "GUNICORN_BACKLOG": 2048,
    },
]


@pytest.fixture(params=conf_data)
def conf(monkeypatch, request):
    for k, v in request.param.items():
        monkeypatch.setenv(k, v)
    gunicorn_conf = GunicornSettings()
    yield gunicorn_conf


@pytest.fixture()
def myapp():
    yield app


def test_guncicorn_serve(conf, myapp):
    class StandaloneApplication(BaseApplication):
        def __init__(self, app, options=None):
            self.options = conf.dict() or {}
            self.application = app
            super().__init__()

        def load_config(self):
            config = {
                key: value
                for key, value in self.options.items()
                if key in self.cfg.settings and value is not None
            }
            for key, value in config.items():
                self.cfg.set(key.lower(), value)

        def load(self):
            return self.application

    StandaloneApplication(myapp, conf).run()
