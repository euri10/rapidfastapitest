from starlette.datastructures import QueryParams
from starlette.testclient import TestClient
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root(sort: str):
    return {'sort': sort}

client = TestClient(app)


def test_plus():
    response = client.get(url='/?sort=+A')
    assert response.json()['sort'] == '+A', repr(response.json()['sort'])
    # raises exception on my computer, representation on my computer is ' A'


def test_qp():
    qp = QueryParams('sort=+A')
    v = qp.values()
    assert v == '+A'