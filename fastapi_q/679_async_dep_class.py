import asyncio

import uvicorn
from fastapi import Depends, FastAPI
from starlette.requests import Request


class DependencyClass:
    async def async_dep(self, request: Request):
        await asyncio.sleep(0)
        return False

    def sync_dep(self, request: Request):
        return True


app = FastAPI()
dependency = DependencyClass()

# Error
@app.get('/async-dep')
async def authenticate(r=Depends(dependency.async_dep)):
    s = await r
    return s

# Everything is fine
@app.get('/sync-dep')
def authenticate(r=Depends(dependency.sync_dep)):
    return r


if __name__ == '__main__':
    uvicorn.run("679_async_dep_class:app", reload= True)