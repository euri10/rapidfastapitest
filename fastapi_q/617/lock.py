import sys
from pathlib import Path


class FileLock:
    def __init__(self, lockname: str):
        self.lockpath = Path(sys.path[0]) / f'{lockname}.lock'

        try:
            self.lockpath.touch(exist_ok=False)
            print("Lock taken!")
            # self = super(FileLock).__init__(lockname)
        except FileExistsError as ex:
            print("Someone else has the lock :(")
            raise ex

    def __del__(self):
        try:
            self.lockpath.unlink()
            print("Lock released!")
        except FileNotFoundError as ex:
            print("What happenned to the lock??")
            raise ex

