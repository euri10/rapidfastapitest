import uvicorn
from fastapi import FastAPI
from lock import FileLock


app = FastAPI()
lock = None

@app.on_event("startup")
async def take_lock():
    global lock
    lock = FileLock("fastapi")
    print(lock)


@app.on_event("shutdown")
async def release_lock():
    global lock
    print(lock)
    del lock


@app.get("/")
async def root():
    return {"message": "Hello World"}


if __name__ == '__main__':
    uvicorn.run("617_filelock2:app", port=8001)
