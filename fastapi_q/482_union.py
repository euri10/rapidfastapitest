from typing import Optional, Union

import uvicorn
from fastapi import FastAPI, Body
from pydantic import BaseModel


class Base(BaseModel):
    name: str


class ModelA(Base):
    foo: Optional[str] = None


class ModelB(Base):
    bar: Optional[str] = None


Input = Union[ModelA, ModelB]

api = FastAPI()


@api.post("/foo")
async def foo_post(data: Input = Body(...)):
    pass


if __name__ == "__main__":
    uvicorn.run(app=api)
