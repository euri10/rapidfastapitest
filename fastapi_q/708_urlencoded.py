import pytest
from fastapi import FastAPI, APIRouter
from starlette.testclient import TestClient

app = FastAPI()

router = APIRouter()


@router.get("/test/{value:path}")
def myurlencodedroute(value:str):
    return value


app.include_router(router, prefix=f"/api")

data = [("A", "A"), ("AA / A+", "AA / A+"), ("AA%20%2F%20A%2B", "AA / A+") ]
@pytest.mark.parametrize("v, expected", data)
def test_urlencoded(v, expected):
    print([r.path for r in app.routes])
    with TestClient(app) as client:
        response = client.get(f"/api/test/{v}")
        assert response.status_code == 200
        assert response.json() == expected