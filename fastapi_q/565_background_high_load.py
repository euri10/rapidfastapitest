import uuid

import gunicorn
import uvicorn
from fastapi import FastAPI
from starlette.background import BackgroundTasks
from rethinkdb import RethinkDB

app = FastAPI()
r: RethinkDB = RethinkDB()
r.set_loop_type("asyncio")


@app.on_event("startup")
async def startup():
    connection = await r.connect(db="test")
    tl = await r.table_list().run(connection)
    if "users" not in tl:
        await r.table_create("users").run(connection)


@app.get("/create_load_testing_user")
async def create_load_testing_user(request: Request, background_tasks: BackgroundTasks):

    api_user_1 = {
        "uid": str(uuid.uuid4()),
        "profile": {"name": "API USER 1"},
        "login": {"client_id": str(uuid.uuid4()), "client_secret": str(uuid.uuid4())},
        "is_performance_test_user": True,
    }
    background_tasks.add_task(login_test_performance_insert, api_user_1)

    return api_user_1


async def login_test_performance_insert(api_user_1):
    await r.table("users").insert(api_user_1).run(await r.connect(db="test"))
    return True


# docker run --name some-rethink -p 28015:28015 -d rethinkdb

# ab -n 8000 -c 1000 http://localhost:8000/create_load_testing_user
if __name__ == "__main__":
    uvicorn.run("565_background_high_load:app")
