import base64
import binascii
import logging
from typing import Generator, AsyncGenerator

import pytest
from async_asgi_testclient import TestClient
from starlette import status
from starlette.applications import Starlette
from starlette.authentication import AuthenticationBackend, AuthenticationError, \
    AuthCredentials, SimpleUser
from starlette.endpoints import WebSocketEndpoint
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.websockets import WebSocket


class BasicAuth(AuthenticationBackend):
    async def authenticate(self, request):
        if "Authorization" not in request.headers:
            return None

        auth = request.headers["Authorization"]
        try:
            scheme, credentials = auth.split()
            decoded = base64.b64decode(credentials).decode("ascii")
        except (ValueError, UnicodeDecodeError, binascii.Error):
            raise AuthenticationError("Invalid basic auth credentials")

        username, _, password = decoded.partition(":")
        return AuthCredentials(["authenticated"]), SimpleUser(username)


app = Starlette()
app.add_middleware(AuthenticationMiddleware, backend=BasicAuth())
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)



@app.websocket_route("/yo")
class ChatWebSocketEndpoint(WebSocketEndpoint):
    allow_anonymous = False

    async def on_connect(self, websocket: WebSocket) -> None:
        if not websocket.user.is_authenticated and not self.allow_anonymous:
            await websocket.close(status.WS_1008_POLICY_VIOLATION)
            log.warning(f"unauthenticated connection attempt")
            return
        await super().on_connect(websocket)


@pytest.fixture(scope="session")
def starlette_testapp() -> Generator[Starlette, None, None]:
    yield app

@pytest.fixture

async def asgiclient(starlette_testapp) -> AsyncGenerator[TestClient, None]:
    yield TestClient(starlette_testapp)


@pytest.mark.asyncio
async def test_service(asgiclient: TestClient) -> None:
    async with asgiclient:
        async with asgiclient.websocket_connect("/yo") as session:
            r = await session.receive_text()
            pass